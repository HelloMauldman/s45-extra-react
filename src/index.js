//Base IMports
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Routes} from "react-router-dom"

import AppNavbar from './components/AppNavbar';
// import Counter from "./components/Counter.js"

import "./index.css"
import 'bootstrap/dist/css/bootstrap.min.css';

import App from "./App"
// Page Components
// import Home from "./pages/Home";
// import Courses from "./pages/Courses"
// import Register from "./pages/Register"
// import Login from "./pages/Login"


ReactDOM.render(
  <React.StrictMode>
    <App/>
    {/* <Home /> */}
    {/* <Courses /> */}
    {/* <Counter/> */}
    {/* <Register /> */}
    {/* <Login /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
