import {useState, useEffect} from "react"
import Container from "react-bootstrap/Container"
const Counter = ()=>{
	const [count, setCount] = useState(0)
	//useEffect takes in the following, a function and an array
	useEffect(()=>{
		document.title = `You clicked ${count} times.`
		console.log("Render")
	},[count])
	return(
		<Container fluid>
			<p>You clicked {count} times</p>
			<button onClick={()=>{
				setCount(count+1)
			}}>Click Me!</button>
		</Container>
	)
};

export default Counter;