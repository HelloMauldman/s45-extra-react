import Container from "react-bootstrap/Container"
const NotFound = () =>{
	return(
		<Container fluid>
			<h3>Page Not Found</h3>
			<p>Go back to the <a href="/">Hompage.</a></p>
		</Container>
	)
}

export default NotFound;