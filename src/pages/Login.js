import Form from "react-bootstrap/Form"
import Container from "react-bootstrap/Container"
import Button from "react-bootstrap/Button"
import Col from "react-bootstrap/Col"
import{ useState, useEffect } from "react"

const Login = ()=>{
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";

		if(isEmailNotEmpty && isPasswordNotEmpty){
			setIsDisabled(false)
		}
		else{
			setIsDisabled(true)
		}
	},[email, password])

	const login = (e)=>{
		e.preventDefault()
		alert("You are now Login Successfully!")
	}
	return(
		<Container fluid>
			<h1>Login</h1>
			<Button>Register</Button>
			<Form onSubmit={login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=>{
						setEmail(e.target.value)
					}}></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" required value={password}onChange={(e)=>{
						setPassword(e.target.value)
					}}></Form.Control>
				</Form.Group>
				<Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
			</Form>
		</Container>
	)
};

export default Login;