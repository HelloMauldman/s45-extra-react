import {useState, useEffect} from "react"

import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"

const Register = ()=>{

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [pwConfirm, setPwConfirm] = useState("");
	const [isDisabled, setIsDisabled] = useState(true)

// 	useEffect(()=>{
// 		console.log(email)
// 
// 	},[email])
// 	useEffect(()=>{
// 		console.log(password)
// 
// 	},[password])
// 	useEffect(()=>{
// 		console.log(pwConfirm)
// 
// 	},[pwConfirm])

	useEffect(()=>{
		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !=="";
		let ispwConfirmNotEmpty = pwConfirm !=="";
		let isPasswordMatched = password === pwConfirm;

		// Determine if all conditions are met

		if(isEmailNotEmpty && isPasswordNotEmpty && ispwConfirmNotEmpty && isPasswordMatched){
			setIsDisabled(false);
		}
		else{
			setIsDisabled(true);
		}
	},[email, password, pwConfirm])

	const register = (e)=>{
		e.preventDefault()
		alert("Registration Successful!")
	}

	return(
		<Container fluid>
			<h3>Register</h3>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
	                <Form.Control type="email" placeholder="Enter email" required value={email} onChange ={(e)=>{
	                	setEmail(e.target.value)
	                }}></Form.Control>
	            </Form.Group>
	                <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>	
				<Form.Group>
				    <Form.Label>Password</Form.Label>
	                <Form.Control type="password" placeholder="Password" required value={password} onChange ={(e)=>{
	                	setPassword(e.target.value)
	                }}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
	                <Form.Control type="password" placeholder="Verify Password" required value={pwConfirm}onChange={(e)=>{
	                	setPwConfirm(e.target.value)
	                }}/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
		
	);
}

export default Register;